/*
 * Module to read PCI mapped registers.
 *
 * Copyright (c) 2018 Francis Laniel <francis.laniel@lip6.fr>
 *
 * This file is part of project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/pci.h>

/**
 * The MCHBAR registers are PCI mapped registers for Intel processor.
 * 0x8086 is the ID for Intel in PCI world.
 */
static unsigned int vendor = PCI_VENDOR_ID_INTEL;
module_param(vendor, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(vendor, "The PCI mapped devices belongs to a vendor.");

/**
 * The device ID of the MCHBAR for i7-7500U is 0x5904.
 */
static unsigned int device = 0x5904;
module_param(device, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(device, "The PCI mapped registers are organized in device.");

/**
 * We want to read the MCHBAR register whom offset is 0x4c40 because it contains
 * information about the power down mode.
 */
static int where = 0x4c40;
module_param(where, int, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(where, "The PCI mapped registers have address.");

/**
 * Search for a specific PCI device.
 * @param vendor The identifier of the vendor of the device.
 * @param device The identifier of the device for this vendor.
 * @ret A pointer on a PCI device if one was found or NULL otherwise.
 */
struct pci_dev *search_pci_dev(unsigned int vendor, unsigned int device)
{
	struct pci_dev *dev;

	dev = pci_get_device(vendor, device, NULL);

	if (!dev){
		pr_err("No pci_dev was found for vendor %x and device %x\n", vendor,
						 device);

		return NULL;
	}

	return dev;
}

/**
 * Read a long from the given PCI mapped device.
 * If an error occur a message will be printed.
 * @param dev The PCI mapped device to read.
 * @param where The location to read in the device.
 * @ret The read value.
 */
u32 read_long(struct pci_dev *dev, int where)
{
	u32 val;
	int ret;

	if ((ret = pci_read_config_dword(dev, where, &val)) != PCIBIOS_SUCCESSFUL)
		pr_err("Problem while reading PCI device: %d.\n", ret);

	return val;
}

/**
 * Init function of the module.
 * @ret 0 if success -ENXIO otherwise.
 */
static int __init mod_init(void)
{
	struct pci_dev *dev;

	dev = search_pci_dev(vendor, device);

	if (!dev)
		return -ENXIO;

	pr_debug("Vendor %u\n", dev->vendor);

	pr_info("Read(vendor = 0x%x, device = 0x%x, offset = 0x%x) = %x\n", vendor, device,
					where, read_long(dev, where));

	return 0;
}

/**
 * Exit function of the module.
 */
static void __exit mod_exit(void)
{

}

module_init(mod_init);
module_exit(mod_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Francis Laniel <francis.laniel@lip6.fr>");
MODULE_DESCRIPTION("Module to read PCI mapped registers.");