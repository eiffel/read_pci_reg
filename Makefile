obj-m += read_pci_reg.o
KERNELDIR = ../linux-arch

all: test
	make -C $(KERNELDIR) M=$(PWD) modules
test:
	cppcheck --enable=all read_pci_reg.c
clean:
	make -C $(KERNELDIR) M=$(PWD) clean